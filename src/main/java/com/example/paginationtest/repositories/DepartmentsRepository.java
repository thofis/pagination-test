package com.example.paginationtest.repositories;

import com.example.paginationtest.model.DepartmentsEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentsRepository extends PagingAndSortingRepository<DepartmentsEntity, String> {


}
