package com.example.paginationtest.repositories;

import com.example.paginationtest.model.EmployeesEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends PagingAndSortingRepository<EmployeesEntity, Integer> {

    @Query(nativeQuery = true,
            value = "select e.emp_no, d.dept_no from employees e " +
                    "inner join dept_emp de on e.emp_no = de.emp_no " +
                    "inner join departments d on de.dept_no = d.dept_no " +
                    "where (e.first_name like %:first_name% " +
                    "or e.last_name like %:first_name% " +
                    "or d.dept_name like %:first_name%) " +
                    "and COALESCE(:birth_date, e.birth_date) = e.birth_date",
            countQuery = "select count(*) from employees e " +
                    "inner join dept_emp de on e.emp_no = de.emp_no " +
                    "inner join departments d on de.dept_no = d.dept_no " +
                    "where (e.first_name like %:first_name% " +
                    "or e.last_name like %:first_name% " +
                    "or d.dept_name like %:first_name%) " +
                    "and COALESCE(:birth_date, e.birth_date) = e.birth_date"

    )
    public Page<Object[]> getEmpNos(String first_name, String birth_date, Pageable pageable);

}
