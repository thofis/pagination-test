package com.example.paginationtest.controller;

import com.example.paginationtest.model.DepartmentsEntity;
import com.example.paginationtest.model.EmployeesEntity;
import com.example.paginationtest.repositories.DepartmentsRepository;
import com.example.paginationtest.repositories.EmployeeRepository;
import lombok.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employees")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeRepository employeeRepository;
    private final DepartmentsRepository departmentsRepository;

    @GetMapping
    public ResponseEntity<Page<EmployeesEntity>> employees(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size
    ) {

        Pageable firstPageWithTenElements = PageRequest.of(page, size);
        Page<EmployeesEntity> firstPage = employeeRepository.findAll(firstPageWithTenElements);
        return ResponseEntity.ok(firstPage);
    }


    @GetMapping("/empnos")
    ResponseEntity<Page<Nos>> empNos(
            @RequestParam(defaultValue = "") String firstName,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size
    ) {
        Page<Nos> empNos = employeeRepository.getEmpNos(firstName, null, PageRequest.of(page, size))
                .map(oa -> new Nos((int)oa[0], (String)oa[1]));
        return ResponseEntity.ok(empNos);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    private class Nos {
        private int empNo;
        private String deptNo;
    }

    @GetMapping("empsanddeps")
    ResponseEntity<Page<EmpAndDep>> empsAndDeps(
            @RequestParam(defaultValue = "") String firstName,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(required = false) String birthDate
    ) {
        Page<Object[]> objects = employeeRepository.getEmpNos(firstName, birthDate, PageRequest.of(page, size));
        Page<EmpAndDep> empsAndDeps = objects.map( object -> {

            if (firstName == null || firstName.length() < 4) {
                throw new BadRequestException("search term has to be at least 4 characters long.");
            }

            Nos nos = new Nos((int)object[0], (String)object[1]);
            EmployeesEntity emp = employeeRepository.findById(nos.empNo).orElseThrow(NotFoundException::new);
            DepartmentsEntity dep = departmentsRepository.findById(nos.deptNo).orElseThrow(NotFoundException::new);
            return EmpAndDep
                    .builder()
                    .employee(emp)
                    .department(dep)
                    .build();
        });
        return ResponseEntity.ok(empsAndDeps);
    }
}
