package com.example.paginationtest.controller;

import com.example.paginationtest.model.DepartmentsEntity;
import com.example.paginationtest.model.EmployeesEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmpAndDep {

    private EmployeesEntity employee;

    private DepartmentsEntity department;

}
