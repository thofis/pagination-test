package com.example.paginationtest;

import com.example.paginationtest.repositories.EmployeeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeTests {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void countEmployees() {
        long nrOfEmployees = employeeRepository.count();
        assertThat(nrOfEmployees).isGreaterThan(100);
    }

}
