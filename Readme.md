* container starten

docker run --name employees-mariadb \
-v /home/dev/code/java/pagination-test/db/data:/var/lib/mysql \
-v /home/dev/code/java/pagination-test/db/init:/docker-entrypoint-initdb.d \
-e MYSQL_ROOT_PASSWORD=123456 -p 13306:3306 -d mariadb:10 

* employees test-db clonen

git clone https://github.com/datacharmer/test_db.git ~/code/java/pagination-test/db/init

* db initialisieren

docker exec -it employees-mariadb bash
# cd /docker-entrypoint-initdb.d
# mysql -uroot -p123456 < employees.sql

* httpie examples 

http localhost:8097/employees?page=5&size=1

http "localhost:8097/employees/empnos?firstName=Georg"

http "localhost:8097/employees/empsanddeps?firstName=Georg&page=0&size=20"

* maria-db docker container as systemd unit:

```
$ sudo cat /etc/systemd/system/employees-db.service 
[Unit]
Description=mariadb-container with example database employees
After=docker.service
Requires=docker.service

[Service]
TimeoutStartSec=0
Restart=no
ExecStartPre=-/usr/bin/docker stop %n
ExecStartPre=-/usr/bin/docker rm %n
ExecStart=/usr/bin/docker run --name %n -v /home/dev/code/java/pagination-test/db/data:/var/lib/mysql -v /home/dev/code/java/pagination-test/db/init:/docker-entrypoint-initdb.d -e MYSQL_ROOT_PASSWORD=123456 -p 13306:3306 -d mariadb:10 
User=dev
Group=dev

[Install]
WantedBy=multi-user.target

```
