#!/bin/bash

docker stop employees-mariadb

docker rm employees-mariadb

docker run --name employees-mariadb \
-v /home/dev/code/java/pagination-test/db/data:/var/lib/mysql \
-v /home/dev/code/java/pagination-test/db/init:/docker-entrypoint-initdb.d \
-e MYSQL_ROOT_PASSWORD=123456 -p 13306:3306 -d mariadb:10

